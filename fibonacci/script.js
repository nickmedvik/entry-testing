function fibonacci(n) {
  let [a, b] = [ 1, 1];
  for (let i = 3; i <= n; i++) {
    let c = a + b;
    [a, b] = [b, c]
  }
  return b;
}

fibonacci(333);
